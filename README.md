<div style="text-align: center;" class="header">
	<b style="color:#cc6600; font-size:2em;">Big Data Analytics Praktikum 5</b><br/>
	<i> Khan / Kolombar /</i>
</div>
<hr>


## Apache Spark – Kafka Data Frames und Structured Streaming* Teil I 


__Tabellen im Parquet File Format –
column based__
* yellow_tripdata_1to11 – alle Taxidaten des Jahres 2015 von Januar bis einschließlich November,
* Datenvolumen (im csv-Format): 20,3 GB
* yellow_tripdata_09 – alle Taxidaten vom September 2015 (entspricht yellow_tripdata von P1)
* Datenvolumen (im csv-Format): 1,7 GB
* yellow_tripdata_12 – alle Taxidaten vom Dezember 2015
* Datenvolumen (im csv-Format): 1,7 GB

__Tabelle im zeilen-orientierten File Format 
– row based__
* yellow_tripdata_1to11_row (Datenvolumen s.o.)

__Window 
Functions__

* Used to perform data analysis calculations
* Address an important need compared to GROUP By clause to return the underlying data
* OVER clause determines windows (sets of rows)
* PARTITION BY- splits the result set in to partitions on which the Window Function is applied


__-Functions
-__
* Aggregate- COUNT, SUM, AVG, MIN, MAX
* Offset- FIRST_VALUE, LAST_VALUE, LEAD, LAG
* Statistical- PERCENT_RANK, CUME_DIST, PERCENTILE_COUNT, PERCENTILE_DISC

__-Frames
-__

* Rows
* Range


## Aufgabe 1 – Die „Top 10“ teuersten bzw. weitesten Fahrten eines Tages pro Stunde [SQL Window] 

__a) Formulieren Sie eine native SQL-Abfrage mit Hilfe von SQL Windows, die für den 01. September
2015 pro Stunde die „Top 10“ der teuersten Fahrten ermittelt. Basis für den Preis
einer Fahrt ist das Merkmal total_amount. Führen Sie diese Anweisung mit Spark SQL aus.__

```
sql("SELECT total_amount, tpep_dropoff_datetime, rank() 
OVER (PARTITION BY window(tpep_dropoff_datetime, '1 hour') 
order by total_amount DESC) as result from yellow_tripdata_09 where Day(tpep_dropoff_datetime)=1 
AND total_amount >0 having result BETWEEN 1 AND 10").show(100)
```

```sql
+------------+---------------------+------+
|total_amount|tpep_dropoff_datetime|result|
+------------+---------------------+------+
|       70.01| 2015-10-01 07:27:...|     1|
|        29.1| 2015-10-01 07:05:...|     2|
|       28.56| 2015-10-01 07:53:...|     3|
|       21.23| 2015-10-01 07:33:...|     4|
|       18.96| 2015-10-01 07:54:...|     5|
|        17.8| 2015-10-01 07:43:...|     6|
|        17.6| 2015-10-01 07:01:...|     7|
|        17.6| 2015-10-01 07:58:...|     7|
|       17.16| 2015-10-01 07:21:...|     9|
|        12.8| 2015-10-01 07:43:...|    10|
|       208.3| 2015-09-01 18:03:...|     1|
|      188.13| 2015-09-01 18:15:...|     2|
|      171.95| 2015-09-01 18:34:...|     3|
|      171.62| 2015-09-01 18:30:...|     4|
|       165.3| 2015-09-01 18:26:...|     5|
|      150.96| 2015-09-01 18:34:...|     6|
|       148.8| 2015-09-01 18:33:...|     7|
|      147.59| 2015-09-01 18:39:...|     8|
|      147.11| 2015-09-01 18:50:...|     9|
|      135.84| 2015-09-01 18:40:...|    10|
|       500.3| 2015-09-01 07:12:...|     1|
|       220.3| 2015-09-01 07:02:...|     2|
|       154.9| 2015-09-01 07:33:...|     3|
|      138.09| 2015-09-01 07:40:...|     4|
|       135.0| 2015-09-01 07:51:...|     5|
|       125.3| 2015-09-01 07:14:...|     6|
|      119.05| 2015-09-01 07:22:...|     7|
|      104.55| 2015-09-01 07:20:...|     8|
|      104.34| 2015-09-01 07:14:...|     9|
|      103.85| 2015-09-01 07:37:...|    10|


```

```
sql("SELECT trip_distance, tpep_dropoff_datetime, rank() 
OVER (PARTITION BY window(tpep_dropoff_datetime, '1 hour')
order by trip_distance DESC)
as result from yellow_tripdata_09 
where Day(tpep_dropoff_datetime)=1 
having result BETWEEN 1 AND 10").show(100)
```

```sql
+-------------+---------------------+------+
|trip_distance|tpep_dropoff_datetime|result|
+-------------+---------------------+------+
|         16.0| 2015-10-01 07:27:...|     1|
|         6.33| 2015-10-01 07:05:...|     2|
|         5.06| 2015-10-01 07:53:...|     3|
|         3.92| 2015-10-01 07:33:...|     4|
|         3.76| 2015-10-01 07:01:...|     5|
|         3.22| 2015-10-01 07:54:...|     6|
|          2.4| 2015-10-01 07:43:...|     7|
|         2.15| 2015-10-01 07:43:...|     8|
|         1.92| 2015-10-01 07:58:...|     9|
|         1.68| 2015-10-01 07:21:...|    10|
|        46.56| 2015-09-01 18:03:...|     1|
|         38.1| 2015-09-01 18:15:...|     2|
|        37.79| 2015-09-01 18:05:...|     3|
|         33.7| 2015-09-01 18:34:...|     4|
|         30.5| 2015-09-01 18:16:...|     5|
|         30.0| 2015-09-01 18:46:...|     6|
|        28.85| 2015-09-01 18:01:...|     7|
|         28.6| 2015-09-01 18:29:...|     8|
|        28.26| 2015-09-01 18:47:...|     9|
|         28.2| 2015-09-01 18:32:...|    10|
|        33.31| 2015-09-01 07:14:...|     1|
|        32.07| 2015-09-01 07:49:...|     2|
|         30.7| 2015-09-01 07:02:...|     3|
|        30.34| 2015-09-01 07:33:...|     4|
|        28.28| 2015-09-01 07:29:...|     5|
|         28.1| 2015-09-01 07:20:...|     6|
|         28.1| 2015-09-01 07:32:...|     6|
|        27.73| 2015-09-01 07:09:...|     8|
|        26.52| 2015-09-01 07:40:...|     9|
|        25.02| 2015-09-01 07:22:...|    10|

```



## Aufgabe 2 –Statistische Kennzahlen über Zeitintervallen [Spark window – vgl.Praktikum 4]

```

sql("""select avg(total_amount), window(tpep_dropoff_datetime, '30 minutes') 
from yellow_tripdata_09 where Day(tpep_dropoff_datetime)=1 
group by window(tpep_dropoff_datetime, '30 minutes')order by 2""").show(48,false)
```

```sql
+------------------+---------------------------------------------+
|avg(total_amount) |window                                       |
+------------------+---------------------------------------------+
|8.8               |[2010-01-01 02:30:00.0,2010-01-01 03:00:00.0]|
|12.793554157005632|[2015-09-01 00:00:00.0,2015-09-01 00:30:00.0]|
|17.40149573690717 |[2015-09-01 00:30:00.0,2015-09-01 01:00:00.0]|
|17.187180952381624|[2015-09-01 01:00:00.0,2015-09-01 01:30:00.0]|
|16.710428809325563|[2015-09-01 01:30:00.0,2015-09-01 02:00:00.0]|
|15.757002176278258|[2015-09-01 02:00:00.0,2015-09-01 02:30:00.0]|
|16.599056224899236|[2015-09-01 02:30:00.0,2015-09-01 03:00:00.0]|
|16.60989778534896 |[2015-09-01 03:00:00.0,2015-09-01 03:30:00.0]|
|16.53174329501901 |[2015-09-01 03:30:00.0,2015-09-01 04:00:00.0]|
|16.56101569713734 |[2015-09-01 04:00:00.0,2015-09-01 04:30:00.0]|
|20.286679425836994|[2015-09-01 04:30:00.0,2015-09-01 05:00:00.0]|
|21.059895833333083|[2015-09-01 05:00:00.0,2015-09-01 05:30:00.0]|
|16.908486238531815|[2015-09-01 05:30:00.0,2015-09-01 06:00:00.0]|
|14.995626297578248|[2015-09-01 06:00:00.0,2015-09-01 06:30:00.0]|
|13.403133769879295|[2015-09-01 06:30:00.0,2015-09-01 07:00:00.0]|
|13.627257575758744|[2015-09-01 07:00:00.0,2015-09-01 07:30:00.0]|
|14.005050543342271|[2015-09-01 07:30:00.0,2015-09-01 08:00:00.0]|
|14.093720903030912|[2015-09-01 08:00:00.0,2015-09-01 08:30:00.0]|
|14.653602105263715|[2015-09-01 08:30:00.0,2015-09-01 09:00:00.0]|
|15.174717325227723|[2015-09-01 09:00:00.0,2015-09-01 09:30:00.0]|
|15.574000209599456|[2015-09-01 09:30:00.0,2015-09-01 10:00:00.0]|
|15.529061843124868|[2015-09-01 10:00:00.0,2015-09-01 10:30:00.0]|
|15.280645627561068|[2015-09-01 10:30:00.0,2015-09-01 11:00:00.0]|
|15.266325970299746|[2015-09-01 11:00:00.0,2015-09-01 11:30:00.0]|
|15.749227767267131|[2015-09-01 11:30:00.0,2015-09-01 12:00:00.0]|
|15.597330827068083|[2015-09-01 12:00:00.0,2015-09-01 12:30:00.0]|
|16.087938727938795|[2015-09-01 12:30:00.0,2015-09-01 13:00:00.0]|
|15.655251933443841|[2015-09-01 13:00:00.0,2015-09-01 13:30:00.0]|
|15.686481151015547|[2015-09-01 13:30:00.0,2015-09-01 14:00:00.0]|

```

## Apache Spark – Kafka Data Frames und Structured Streaming* Teil II

__Aufgabe 4
– Nutzung von Kafka als Producer und Consumer von Data Streams__
## a)

## Producer
```/opt/bd/kafka/bin/kafka-console-producer.sh --broker-list localhost:9984 --topic bda-gruppe1-topic```

## Consumer
```/opt/bd/kafka/bin/kafka-console-consumer.sh --zookeeper yoda.fbi.hda.de:2181 --topic bda-gruppe1-topic --from-beginning```

__Die ```--from-beginning``` 
Option zeigt in der Konsole alle im Kafka gespeicherten Messages.
Ohne diese Option werden nur die aktuellsten Messages angezeigt.__

## b)
Lesen des Kafka streams mit Spark.

```
val topic_kafka_stream =
spark.readStream.format("kafka")
.option("kafka.bootstrap.servers","yoda.fbi.h-da.de:9984")
.option("subscribe"," bda-gruppe1-topic")
.option("startingOffsets","earliest")
.option("kafkaConsumer.pollTimeoutMs","8192").load()

```
Ausgabe des Schematas des DataFrames, der den Stream repräsentieren
```topic_kafka_stream.printSchema ```
```
scala> topic_kafka_stream.printSchema
root
 |-- key: binary (nullable = true)
 |-- value: binary (nullable = true)
 |-- topic: string (nullable = true)
 |-- partition: integer (nullable = true)
 |-- offset: long (nullable = true)
 |-- timestamp: long (nullable = true)
 |-- timestampType: integer (nullable = true)

```


Word count Query
``` 
 
val topic_kafka_stream =
spark.readStream.format("kafka")
.option("kafka.bootstrap.servers","yoda.fbi.h-da.de:9984")
.option("subscribe"," bda-gruppe1-topic")
.option("startingOffsets","earliest")
.option("kafkaConsumer.pollTimeoutMs","8192").load()



val df = topic_kafka_stream.select(explode(split($"value".cast("string"), "\\s+")).as("word")).groupBy($"word").count.orderBy(desc("count"))
val writer =  df.writeStream.option("truncate",false).queryName("khan").outputMode("complete").format("console").start()

```
##Result
```SQL
+------------+-----+
|word        |count|
+------------+-----+
|Khan        |835  |
|Liebe       |765  |
|bei         |545  |
|BIGDATA     |416  |
|jabbalapappa|408  |
|Ich         |243  |
|FrauSchestag|86   |
|a           |51   |
|234         |41   |
|            |38   |
|g           |26   |
|f           |24   |
|januar      |21   |
|das         |21   |
|test        |19   |
|sa          |18   |
|ich         |14   |
|saf         |14   |
|jabbalappapa|8    |
|as          |7    |
+------------+-----+
only showing top 20 rows
```


## 5 Aufgabe 5 – Nutzung von Kafka als Datenquelle für Spark mit den NY Taxidaten des Monats Dezember 2015 als „real time“ Datenstrom

```
spark.conf.set("spark.sql.shuffle.partitions", "200") // führt zu fehlern bei geringere Anzahl?!

case class Ride(val vendorID:String, val tpep_pickup_datetime: String, val
tpep_dropoff_datetime: String, val passenger_count: String, val trip_distance: String, val pickup_longitude: String, val pickup_latitude:
String, val RatecodeID: String, val store_and_fwd_flag: String, val
dropoff_longitude: String, val dropoff_latitude: String, val payment_type:
String, val fare_amount: String, val extra: String, val mta_tax: String, val tip_amount: String,val tolls_amount: String, val improvement_surcharge:
String, val total_amount:String) extends Serializable {
override def toString(): String = {
"%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n".format(vendorID,tpep_pickup_datetime,tpep_dropoff_datetime,passenger_count,trip_distance,pickup_longitude,pickup_latitude,RatecodeID,store_and_fwd_flag,dropoff_longitude,dropoff_latitude,payment_type,fare_amount,extra,mta_tax,tip_amount,tolls_amount,improvement_surcharge,total_amount)
}

def this(in: String) {
this(in.split(",")(0), in.split(",")(1), in.split(",")(2),
in.split(",")(3), in.split(",")(4), in.split(",")(5), in.split(",")(6),
in.split(",")(7), in.split(",")(8), in.split(",")(9), in.split(",")(10),
in.split(",")(11), in.split(",")(12), in.split(",")(13),
in.split(",")(14), in.split(",")(15), in.split(",")(16),
in.split(",")(17), in.split(",")(18))
}
}

val topicTaxi =
 spark.readStream.format("kafka")
.option("kafka.bootstrap.servers","yoda.fbi.h-da.de:9984")
.option("subscribe","yellow_tripdata_12")
.option("startingOffsets","earliest")
.option("kafkaConsumer.pollTimeoutMs","8192").load()

val values = topicTaxi.select("value").as[String].map(new Ride(_))

val writer =  values.writeStream.option("truncate",false).queryName("kolombar").outputMode("append").format("console").start()
```

## Hier werden alle Events im append Mode in der Konsole ausgegeben
``` sql

scala> -------------------------------------------
Batch: 0
-------------------------------------------
+--------+--------------------+---------------------+---------------+-------------+-------------------+------------------+----------+------------------+-------------------+------------------+------------+-----------+-----+-------+----------+------------+---------------------+------------+
|vendorID|tpep_pickup_datetime|tpep_dropoff_datetime|passenger_count|trip_distance|pickup_longitude   |pickup_latitude   |RatecodeID|store_and_fwd_flag|dropoff_longitude  |dropoff_latitude  |payment_type|fare_amount|extra|mta_tax|tip_amount|tolls_amount|improvement_surcharge|total_amount|
+--------+--------------------+---------------------+---------------+-------------+-------------------+------------------+----------+------------------+-------------------+------------------+------------+-----------+-----+-------+----------+------------+---------------------+------------+
|1       |2015-12-01 00:00:01 |2015-12-01 00:05:56  |1              |1.20         |-73.993934631347656|40.741683959960937|1         |N                 |-73.997665405273438|40.747467041015625|1           |6.5        |0.5  |0.5    |0.2       |0           |0.3                  |8           |
|2       |2015-12-01 00:00:04 |2015-12-01 00:10:43  |2              |1.42         |-73.9996337890625  |40.734771728515625|1         |N                 |-73.989067077636719|40.723121643066406|1           |8.5        |0.5  |0.5    |2.45      |0           |0.3                  |12.25       |
|2       |2015-12-01 00:00:05 |2015-12-01 00:15:12  |1              |1.77         |-73.975898742675781|40.7559814453125  |1         |N                 |-73.97711181640625 |40.750175476074219|1           |11.5       |0.5  |0.5    |3.84      |0           |0.3                  |16.64       |

```



```
spark.conf.set("spark.sql.shuffle.partitions", "200") // führt zu fehlern bei geringere Anzahl?!

case class Ride(val vendorID:String, val tpep_pickup_datetime: String, val
tpep_dropoff_datetime: String, val passenger_count: String, val trip_distance: String, val pickup_longitude: String, val pickup_latitude:
String, val RatecodeID: String, val store_and_fwd_flag: String, val
dropoff_longitude: String, val dropoff_latitude: String, val payment_type:
String, val fare_amount: String, val extra: String, val mta_tax: String, val tip_amount: String,val tolls_amount: String, val improvement_surcharge:
String, val total_amount:String) extends Serializable {
override def toString(): String = {
"%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n".format(vendorID,tpep_pickup_datetime,tpep_dropoff_datetime,passenger_count,trip_distance,pickup_longitude,pickup_latitude,RatecodeID,store_and_fwd_flag,dropoff_longitude,dropoff_latitude,payment_type,fare_amount,extra,mta_tax,tip_amount,tolls_amount,improvement_surcharge,total_amount)
}

def this(in: String) {
this(in.split(",")(0), in.split(",")(1), in.split(",")(2),
in.split(",")(3), in.split(",")(4), in.split(",")(5), in.split(",")(6),
in.split(",")(7), in.split(",")(8), in.split(",")(9), in.split(",")(10),
in.split(",")(11), in.split(",")(12), in.split(",")(13),
in.split(",")(14), in.split(",")(15), in.split(",")(16),
in.split(",")(17), in.split(",")(18))
}
}

val topicTaxi =
 spark.readStream.format("kafka")
.option("kafka.bootstrap.servers","yoda.fbi.h-da.de:9984")
.option("subscribe","yellow_tripdata_12")
.option("startingOffsets","earliest")
.option("kafkaConsumer.pollTimeoutMs","8192").load()

val values = topicTaxi.select("value").as[String].map(new Ride(_))
val dirtyValues = values.filter($"trip_distance" <= 0)


val dirtyQuery = dirtyValues.writeStream.format("parquet").option("path","hdfs://141.100.62.105:54310/user/istjbkhan/falseDec").option("checkpointLocation","/tmp/khanJ/").start

val writer =  dirtyValues.writeStream.option("truncate",false).queryName("kolombar").outputMode("append").format("console").start()

```






## Ausgabe der dirtyValues trip_distance <=0. Wir filtern alle Werte mit der Trip Distanz <= 0


Einlesen der im Parquet Format gespeicherten Daten auf hdfs zeigt die gefilterten Daten.

```

val dirtyData =spark.sqlContext.read.parquet("hdfs://141.100.62.105:54310/user/istjbkhan/falseDec/")
dirtyData.show()
```


``` sql
+--------+--------------------+---------------------+---------------+-------------+-------------------+-----
|vendorID|tpep_pickup_datetime|tpep_dropoff_datetime|passenger_count|trip_distance|pickup_longitude   |picka
+--------+--------------------+---------------------+---------------+-------------+-------------------+-----
|2       |2015-12-04 19:09:25 |2015-12-04 19:09:47  |3              |.00          |-73.97894287109375 |40.76
|1       |2015-12-04 19:10:16 |2015-12-04 19:10:16  |1              |.00          |-73.979705810546875|40.73
|2       |2015-12-04 19:10:33 |2015-12-04 19:11:39  |3              |.00          |-73.979072570800781|40.90  
|1       |2015-12-04 17:24:50 |2015-12-04 17:24:50  |1              |.00          |-73.979400634765625|40.78
|2       |2015-12-04 19:13:47 |2015-12-04 19:13:50  |2              |.00          |-73.789627075195313|40.64
|1       |2015-12-04 17:33:16 |2015-12-04 17:33:30  |1              |.00          |-73.783134460449219|40.64
|2       |2015-12-04 19:20:18 |2015-12-04 19:26:24  |1              |.00          |-73.983612060546875|40.72               
|2       |2015-12-04 19:25:52 |2015-12-04 19:25:59  |1              |.00          |-73.776329040527344|40.64
|1       |2015-12-04 19:26:54 |2015-12-04 19:27:41  |1              |.00          |-73.994148254394531|40.66
|1       |2015-12-04 19:29:49 |2015-12-04 19:29:49  |1              |.00          |-73.929275512695313|40.79  
|2       |2015-12-04 19:32:32 |2015-12-04 19:32:37  |1              |.00          |0                  |0             
|1       |2015-12-04 19:35:16 |2015-12-04 19:35:19  |1              |.00          |-73.942649841308594|40.79      
|1       |2015-12-04 19:33:48 |2015-12-04 19:33:53  |1              |.00          |-74.001602172851562|40.72    
|1       |2015-12-04 19:35:35 |2015-12-04 19:35:37  |1              |.00          |-73.951316833496094|40.77  
|2       |2015-12-04 19:44:50 |2015-12-04 19:46:36  |1              |.00          |-74.001960754394531|40.73      
|2       |2015-12-04 19:44:56 |2015-12-04 19:45:16  |2              |.00          |-73.980972290039063|40.75
|2       |2015-12-04 19:36:47 |2015-12-04 19:36:50  |2              |.00          |-73.9764404296875  |40.68
|1       |2015-12-04 19:40:17 |2015-12-04 19:40:20  |1              |.00          |-74.001068115234375|40.74
|1       |2015-12-04 19:37:45 |2015-12-04 19:37:45  |1              |.00          |-73.979698181152344|40.76
|1       |2015-12-04 19:48:59 |2015-12-04 19:50:26  |1              |.00          |-73.9552001953125  |40.77
+--------+--------------------+---------------------+---------------+-------------+-------------------+-----
only showing top 20 rows

```



